const data = [
    { 
        place: "Melbourne", 
        country: "Australia", 
        location: {
            lat: '99',
            lng: '88'
        },
        temperature: '38 Degree Celsius'
    },{ 
        place: "New Delhi", 
        country: "India", 
        location: {
            lat: '84',
            lng: '44'
        },
        temperature: '42 Degree Celsius'
    },{ 
        place: "Pretoria", 
        country: "SouthAfrica", 
        location: {
            lat: '35',
            lng: '24'
        },
        temperature: '39 Degree Celsius'
    },{ 
        place: "Mexico City", 
        country: "Mexico", 
        location: {
            lat: '34',
            lng: '38'
        },
        temperature: '43 Degree Celsius'
    },
    { 
        place: "London", 
        country: "England", 
        location: {
            lat: '57',
            lng: '34'
        },
        temperature: '26 Degree Celsius'
    }
]

//q1

const res=data.reduce((pre,cur)=>{
    let b={}
    const {place,location}=cur
    b={[place]:`(${location.lat},${location.lng})`}
    pre.push(b)
    return pre

},[])
console.log(res);


//q2
let a=data.sort(function(a,b){
    if(a.temperature>b.temperature){
        return 1
    }
    else{
        return -1
    }
})
console.log(a);


//q3

const result=data.reduce((pre,cur)=>{
    let b={}
    const {country,place,location}=cur
    b={[country]:{
        [place]:{location}
    }}
    pre.push(b)
    return pre

},[])
console.log(result);


//q4
data.forEach(element => {
    if (element.country=="SouthAfrica"){
        return element.temperature="49 Degree Celsius"
    }
});
console.log(data)

//q5

const del={
    place:"bangalore",country:"india",location:{lat:'84',lng:'87'},temperature:'29 degree celcius'
}
data.splice(3,0,a)
console.log(data);


//q6
const dele=data.splice(2,1)
console.log(data);

//q7

[data[1],data[data.length-2]]=[data[data.length-2],data[1]]
console.log(data);